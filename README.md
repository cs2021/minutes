# Minutes Computational Sciences
This is a living document. Feel free to edit.

[[_TOC_]]

# Minutes tutorial 2
## Dimensional analysis
We calculated the some non dimensional parameters and a nondimensional form of
a reaction diffusion equation, given reference length and scale. We did do that
by simply rescaling x,t by the reference length and time.

## git and gitlab.
For git we specifically covered how to

- clone a repository i.e. `git clone`.
- stage changes for commit `git add FILENAME`.
- commit changes `git commit -m"MESSAGE"`.
- push changes synchronize to a remote i.e. `git push` and `git pull`

To implement some *feature* I discussed the following strategy

- create and switch to new branch `BRANCH_NAME` i.e. `git branch BRANCHNAME`
  followed by `git checkout BRANCHNAME`. The following command provides the
  same functionality `git checkout -b BRANCHNAME`.
- commmit some changes via first staging the changes in possible several files
  `git add CHANGEDFILE` and subsequently executing `git commit -m"MEANINGFUL
  MESSAGE"`.
- if you wish to synchronize with the remote server, then do `git push` and/or
  `git pull`
- iterate on the previous two stepss until you implemented the feature
- merge into the original branch `TARGET_BRANCH` (main is protected) via `git
  checkout TARGET_BRANCH` followed by `git merge BRANCHNAME`.

## Game of Life 
We implemented a very basic (and buggy) version of Conway's Game of Life.

We used python and numpy. **todo: more details**

# Minutes tutorial 3

## git
We repeated some git commands and discussed some basic concepts. We realized
git changes the files in the repository depending on which branch we are in.
The differences to the othere branches and commits are stored in `.git`.
Do not delete the `.git` folder, except you absolutely sure about it.

## Game of Life
We removed some bugs from the example implementation.
We assessed the quality of our first version of GOL.

| Quality criteria | Assessment
| ---              | ---
| Reliability      | +
| Maintainability  | --
| Testability      | --
| Portability      | -
| Reuseability     | --
| Performance      | -
| Readability      | --

Our firsts improvements where guided by the question if our code actually does
the correct thing.

To this end we implemented two test functions and moved pieces of the code
(essentially the function which computes everything for the next step)
into a new module (a.k.a. file) named `gol_library`.

Our first test checked if constant patterns of GoL (named still lifes) are
preserved under application of our update function c.f.
[Wikipedia](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life).  We realized this is
a weak test as we implemented a dummy version of GoL against it.

Subsequently we implement a second  test which checks if the oscillator
patterns are properly resolved. 

We "introduced" `pytest` and `pytest-benchmark`. The latter we used to write a
simple benchmark function for the update function. 

Our improved version of GoL uses a class structure (which was rightfully
critized as superficial at this point). The background on this needs more
clarification the next time **todo**. 

Assessment of our second version (mean):

| Quality criteria | Assessment
| ---              | ---
| Reliability      | +
| Maintainability  | o 
| Testability      | + 
| Portability      | -
| Reuseability     | o
| Performance      | o
| Readability      | o 


### Test driven development
**todo**

# Minutes tutorial 4

## Maintainability & Readability
- Book: Pragmatic programmer

Best practices in languages e.g. for C++
()[https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines]
cppcon Videos

How to structure our code?
- Don't Repeat Yourself!
- Components (modules, classes, etc) should be decoupled and *orthogonal* as possible
  (e.g. do not use global variables)
- Design By Contract e.g. if precondition A is met, postcondition B is true
  (and class invariant C is true when method finishes)
### Classes 
- Class **SHOULD** have an invariant, otherwise you better use plain functions.
- It should, however, have only one responsibility 
- SOLID principles (1,2,4 especially important) 
  - Single responsibility 
  - Open-Closed, open to extension, closed to modification (Remark: not only for inheritance)
  - Liskov substitution (if child class, you should be able to replace by parent)
  - Interface segregation (short, make the interface as lean as possible) 
  - Dependency inversion (depend on abstract versions of some task not the concrete methods)

### Inheritance 
```
class parent:
  def method():
    ...
  ...

class child(parent):
  ...


a = child
a.method()
```
Not the first thing to do. Prefer composition over inheritance.
Inheritance couples components even if not necessary.

### Interfaces & Protocols 
Interface: 
(Class) and the function signature (of its members)

No native support in python for abstract classes a.k.a interfaces use ABC
package if required

A way to statically check the interfaces (before execution of the program code)
are Protocols of the typing package.

In general to specify a full interface in python without documentation is *close to impossible*.
Use type hints, if nothing else helps use doc strings

The interface/protocol states major parts of the *contract* between the
function and the caller.
Rest might be in doc strings or use some library.
Example of a rather good contract:

```python
def sqrt(x:float)-> float:
  """ returns the non negative square root of a non negative number x """
```
## Game of life
We implemented a protocol for a general `Game` and we adapted our previous
implementation to it. We copied some other classes providing the user
interface. Although only one worked (the matplotlib one was faulty) both
consumed our `GameOfLife` implementation without any issues.


## Performance
The following is not *premature optimization*!
Lets name it "performance by design".

### Architecture and floating point pipeline
See pdf in the webex space.

## References
The video I keep talking about. The content is great, the speaker too. To make
it just a little bit more funny one has to realize Mike Acton talks at *the* C++
conference, but indeed advertises to avoid most of the language and essentially
stick to ;-).
(Link to video)[https://www.youtube.com/watch?v=rX0ItVEVjHc]

A very good reference to improve in programming:
Andrew Hunt, David Thomas, *The pragmatic programmer*, 1999, Addison Wesley

# Minutes tutorial 5
## Performance
### More details on numpy 
## ODE integrators 

# Minutes tutorial 6
## ODE integrators 
## Development process etc.

# References
Decent interactive online polling for free (our room):
[Link](https://ars.particify.de/creator/room/77910727)

## Numerical methods for ODEs
Two standard references (quite accessible, but huge) are
- Ernst Hairer, Gerhard Wanner,Syvert P. Nørsett, *Solving Ordinary
  Differential Equations I*, 1993, Springer
- Ernst Hairer, Gerhard Wanner,Syvert P. Nørsett, *Solving Ordinary
  Differential Equations II*, 1993, Springer

For Hamiltonian systems I recommend:
- Benedict Leimkuhler, Sebastian Reich, *Simulating Hamiltonian Dynamics*,
  Camebridge University Press, 2004
- Ernst Hairer, Christian Lubich, Gerhard Wanner, *Geometric Numerical
  Integration: Structure Preserving Algorithms for Ordinary Differential
  Equations*, 2006, Springer



